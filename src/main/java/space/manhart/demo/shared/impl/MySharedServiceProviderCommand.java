package space.manhart.demo.shared.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import space.manhart.demo.shared.api.MySharedServiceProvider;

// COMMENT: changes start
/**
 * This class defines a new Gogo Shell command with the scope greet and the
 * function greet.<br/>
 * Be aware that you can freely choose the 'osgi.command.scope', but the
 * 'osgi.command.function' must match the implementing function name.
 * 
 * @author Manuel Manhart
 *
 */
// @formatter:off
@Component(
    immediate = true,
    property = {
        "osgi.command.scope=greet",
        "osgi.command.function=greet"
    },
    service = Object.class
)
// @formatter:on
public class MySharedServiceProviderCommand {

	/**
	 * Autowire the osgi service
	 */
	@Reference
	private MySharedServiceProvider service;

	/**
	 * function name must match the defined function in @Component annotation
	 * 
	 * @param name
	 */
	public void greet(String name) {
		service.greet(name);
	}
}
// COMMENT: changes end
